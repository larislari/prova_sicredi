package paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BootstrapV4Page extends PaginaBase {
    public BootstrapV4Page(WebDriver navegador) {
        super(navegador);
    }

    //Clique no botão Add Customer
    public AddCustomerPage clickAddCustomer() throws InterruptedException {
        Thread.sleep(1000);
        navegador.findElement(By.xpath("//a[@href='/demo/bootstrap_theme_v4/add']")).click();
        return new AddCustomerPage(navegador);

    }


}
