package paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.junit.Assert.assertEquals;

public class AddCustomerPage extends PaginaBase {
    public AddCustomerPage(WebDriver navegador) {
        super(navegador);
    }

    //Localizar campo nome e preencher
    public AddCustomerPage preencherNome(String name) throws InterruptedException {
        Thread.sleep(1000);
        navegador.findElement(By.id("field-customerName")).sendKeys(name);
        return this;
    }

    //Localizar campo Last Name e preencher
    public AddCustomerPage preencherUltimoNome(String lastName) {
        navegador.findElement(By.id("field-contactLastName")).sendKeys(lastName);
        return this;
    }

    //Localizar campo Contact First Name e preencher
    public AddCustomerPage preencherPrimeiroNome(String contactFirstName) {
        navegador.findElement(By.id("field-contactFirstName")).sendKeys(contactFirstName);
        return this;
    }

    //Localizar campo Phone e preencher
    public AddCustomerPage preencherFone(String phone) {
        navegador.findElement(By.id("field-phone")).sendKeys(phone);
        return this;
    }

    //Localizar campo Address Line 1 e preencher
    public AddCustomerPage preencherEndereco1(String addressLine1) {
        navegador.findElement(By.id("field-addressLine1")).sendKeys(addressLine1);
        return this;
    }

    //Localizar campo Address Line 2 e preencher
    public AddCustomerPage preencherEndereco2(String addressLine2) {
        navegador.findElement(By.id("field-addressLine2")).sendKeys(addressLine2);
        return this;

    }

    //Localizar campo City e preencher
    public AddCustomerPage preencherCidade(String city) {
        navegador.findElement(By.id("field-city")).sendKeys(city);
        return this;
    }

    //Localizar campo State e preencher
    public AddCustomerPage preencherEstado(String state) {
        navegador.findElement(By.id("field-state")).sendKeys(state);
        return this;
    }

    //Localizar campo Postal Code e preencher
    public AddCustomerPage preencherCep(String postalCode) {
        navegador.findElement(By.id("field-postalCode")).sendKeys(postalCode);
        return this;
    }

    //Localizar campo Country e preencher
    public AddCustomerPage preencherPais(String country) {
        navegador.findElement(By.id("field-country")).sendKeys(country);
        return this;
    }

    //Selecionar funcionario
    public AddCustomerPage selecionarFuncionario() throws InterruptedException {
        Thread.sleep(1000);
        navegador.findElement(By.xpath("//*[@id=\"field_salesRepEmployeeNumber_chosen\"]/a")).click();
        navegador.findElement(By.xpath("//*[@id=\"field_salesRepEmployeeNumber_chosen\"]/div/ul/li[8]")).click();
        return this;
    }

    //Localizar campo Credit Limit e preencher
    public AddCustomerPage preencherLimiteCredito(String creditLimit) throws InterruptedException {
        Thread.sleep(3000);
        navegador.findElement(By.id("field-creditLimit")).sendKeys(creditLimit);
        return this;
    }

    //Apertar botão save
    public AddCustomerPage botaoSave() {
        navegador.findElement(By.id("form-button-save")).submit();
        return this;
    }

    //Validação mensagem de tela
    public AddCustomerPage validarMensagem() {
        String mensagem = navegador.findElement(By.xpath("//*[@id=\"report-success\"]/p")).getText().replace(" or", "");
        String segundaMensagem = navegador.findElement(By.xpath("//*[@id=\"report-success\"]/p/a")).getText();
        String terceiraMensagem = navegador.findElement(By.xpath("//*[@id=\"report-success\"]/p/a[2]")).getText();
        String sucesso = mensagem + " " + segundaMensagem + " or " + terceiraMensagem;
        assertEquals("Your data has been successfully stored into the database. Edit Customer or Go back to list",
                sucesso);
        return this;
    }

    public AddCustomerPage quit() {
        navegador.quit();
        return null;
    }


    public AddCustomerPage preencherFormulario(
            String name,
            String lastName,
            String contactFirstName,
            String phone,
            String addressLine1,
            String addressLine2,
            String city,
            String state,
            String postalCode,
            String country,
            String creditLimit


    ) throws InterruptedException {
        return preencherNome(name)
                .preencherUltimoNome(lastName)
                .preencherPrimeiroNome(contactFirstName)
                .preencherFone(phone)
                .preencherEndereco1(addressLine1)
                .preencherEndereco2(addressLine2)
                .preencherCidade(city)
                .preencherEstado(state)
                .preencherCep(postalCode)
                .preencherPais(country)
                .selecionarFuncionario()
                .preencherLimiteCredito(creditLimit)
                .botaoSave()
                .validarMensagem()
                .quit();


    }
}

