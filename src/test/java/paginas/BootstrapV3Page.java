package paginas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BootstrapV3Page extends PaginaBase {
    public BootstrapV3Page(WebDriver navegador) {
        super(navegador);
    }

    //Localizar o combo select version e mudar o valor para “Bootstrap V4 Theme”
    public BootstrapV4Page mudarValorCombo() {
        navegador.findElement(By.id("switch-version-select")).click();
        navegador.findElement(By.xpath("//option[@value='/demo/bootstrap_theme_v4']")).click();

        return new BootstrapV4Page(navegador);
    }
    public BootstrapV4Page mudarVersaoBootstrap() {
        return mudarValorCombo();
    }
}
