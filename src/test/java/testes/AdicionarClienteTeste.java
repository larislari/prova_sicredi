package testes;

import org.easetech.easytest.annotation.DataLoader;
import org.easetech.easytest.annotation.Param;
import org.easetech.easytest.runner.DataDrivenTestRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import paginas.BootstrapV3Page;
import suporte.Web;

@RunWith(DataDrivenTestRunner.class)
@DataLoader(filePaths = "AdicionarClienteTeste.csv")
public class AdicionarClienteTeste {
    private WebDriver navegador;

    @Before
    public void setUp() {
        navegador = Web.criarChrome();
    }

    @Test
    public void adicionarCliente(
            @Param(name = "name") String name,
            @Param(name = "lastName") String lastName,
            @Param(name = "contactFirstName") String contactFirstName,
            @Param(name = "phone") String phone,
            @Param(name = "addressLine1") String addressLine1,
            @Param(name = "addressLine2") String addressLine2,
            @Param(name = "city") String city,
            @Param(name = "state") String state,
            @Param(name = "postalCode") String postalCode,
            @Param(name = "country") String country,
            @Param(name = "creditLimit") String creditLimit

    ) throws InterruptedException {

        new BootstrapV3Page(navegador)
                .mudarVersaoBootstrap()
                .clickAddCustomer()
                .preencherFormulario(
                        name,
                        lastName,
                        contactFirstName,
                        phone,
                        addressLine1,
                        addressLine2,
                        city,
                        state,
                        postalCode,
                        country,
                        creditLimit);
    }
}

